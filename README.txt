Gallery Assist Upport
---------------------

Extends Gallery Assist with the following features:

 - Scan the import folder for images.
 - Detect images-files or zip-files in the import folder.
 - Zip files will be extracted, their contents filtered and found image files are imported into the current edited Gallery.

This version is compatible at Gallery Assist version 1.16-beta2.

Incompatibilities
-----------------
We would be very grateful if you give us feedback in case you get errors or find issues.


Maintainer
----------
The Gallery Assist Upport module was originally developped by:
Juan Carlos Morejon Caraballo

Current maintainer:
Juan Carlos Morejon Caraballo
